import 'package:flutter/material.dart';
import 'package:flutter_minggu5/main.dart';

import 'Tugas12/Telegram.dart';
import 'BelajarImage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        // '/': (context) => Telegram(),
        '/': (context) => BelajarImage(),
      },
    );
  }
}
