import 'dart:io';
main() {
  stdout.write("Nilai a: ");
  double a = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai b: ");
  double b = double.parse(stdin.readLineSync()!);

  double jumlah;

  //penjumlahan
  jumlah = a + b;
  print("$a + $b = $jumlah");
  //pengurangan
  jumlah = a - b;
  print("$a - $b = $jumlah");
  //perkalian
  jumlah = a * b;
  print("$a * $b = $jumlah");
  //pembagian
  jumlah = a / b;
  print("$a / $b = $jumlah");
}